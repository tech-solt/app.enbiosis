import axios from 'axios'
import {app_config} from '../../config'
import {SET_MY_NUTRITION_PROGRAMS} from '../actionTypes'

export const setMyNutritionPrograms = myNutritionPrograms => ({
    type: SET_MY_NUTRITION_PROGRAMS,
    myNutritionPrograms
})

export const handleGetMyNutritionPrograms = kitCode => {
    return dispatch => {
        return new Promise((resolve, reject) => {
            axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.userToken}`;
            axios.defaults.headers.common['Content-Language'] = localStorage.lang;
            return axios.get(`${app_config.api_url}/user/kit/allprogram?kit_code=${kitCode}`)
                .then(res => {
                    dispatch(setMyNutritionPrograms(res.data.data.program))
                    resolve();
                })
                .catch(err => {
                    // console.log(err.response.data);
                    reject(err.response.data.errors);
                })
        })
    }
}

export const handleGenerateNutritionProgram = program => {
    return new Promise((resolve, reject) => {
        axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.userToken}`;
        axios.defaults.headers.common['Content-Language'] = localStorage.lang;
        return axios.post(`${app_config.pdf_api}/pdf/generate-nutrition-program`, {...program}, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then(res => {
                const reportUrl = URL.createObjectURL(convertBase64ToBlob(res.data.program));
                resolve(reportUrl);
            })
            .catch(err => {
                // console.log(err.response);
                reject();
            })
    })
}

// for converting the pdf to a base64 url
const convertBase64ToBlob = (base64String) => {
    const parts = base64String.split(';base64,');
    const decodedData = window.atob(parts[1]);
    const uInt8Array = new Uint8Array(decodedData.length);

    for(let i = 0; i < decodedData.length; i++){
        uInt8Array[i] = decodedData.charCodeAt(i);
    }

    return new Blob([uInt8Array], {type: 'application/pdf'});
}