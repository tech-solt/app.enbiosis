import {SET_MY_NUTRITION_PROGRAMS} from '../actionTypes'

const INITIAL_STATE = {
    myNutritionPrograms: []
}

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case SET_MY_NUTRITION_PROGRAMS:
            return {
                ...state,
                myNutritionPrograms: [...action.myNutritionPrograms]
            }
        default:
            return state;
    }
}