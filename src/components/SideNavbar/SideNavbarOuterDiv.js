import React, { Component, createRef } from 'react'

export default class SideNavbarOuterDiv extends Component {
    state = {
        showResultMenu: false,
        showEndietMenu: false
    }
    result_dd = createRef();
    endiet_dd = createRef();


    componentDidMount(){
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount(){
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    setWrapperRef = (node) => {
        this.wrapperRef = node
    }

    handleClickOutside = (e) => {
        if(this.wrapperRef && !this.wrapperRef.contains(e.target)){
            this.setState({
                showResultMenu: false,
                showEndietMenu: false
            });
        }
    }
    
    handleShowResultMenu = (e) => {
        const {showResultMenu} = this.state;
        this.setState({
            showResultMenu: !showResultMenu,
            result_dd_height: this.result_dd.current.clientHeight
        });
    }

    handleShowEndietMenu = (e) => {
        const {showEndietMenu} = this.state;
        this.setState({
            showEndietMenu: !showEndietMenu,
            endiet_dd_height: this.endiet_dd.current.clientHeight
        });
    }

    render() {
        const {showResultMenu, showEndietMenu, result_dd_height, endiet_dd_height} = this.state;
        const childrenWithAdjustedProps = React.Children.map(this.props.children, child =>
            React.cloneElement(child, {
                showResultMenu,
                handleShowResultMenu: this.handleShowResultMenu,
                result_dd: this.result_dd,
                result_dd_height,
                showEndietMenu,
                handleShowEndietMenu: this.handleShowEndietMenu,
                endiet_dd: this.endiet_dd,
                endiet_dd_height
            })
        );
        return (
            <div ref={this.setWrapperRef}>
                {childrenWithAdjustedProps}
            </div>
        )
    }
}
