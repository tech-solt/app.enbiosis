import React, { Component, Fragment } from 'react'
import './TermsOfService.css'
import {Accordion} from 'react-bootstrap'
import TermsCard from './TermsCard/TermsCard'
import UyelikSozlesmesi from './TermsDocs/UyelikSozlesmesi'
import GizlilikPolitikasi from './TermsDocs/GizlilikPolitikasi'
import KullanimSartlari from './TermsDocs/KullanimSartlari'
import AydinlatmaFormu from './TermsDocs/AydinlatmaFormu'

class TermsOfService extends Component {
    state = {
        activeKey: "0"
    }

    handleAccordionCollapse = (activeKey) => {
        this.setState({
            activeKey
        });
    };

    render(){
        const {translate, userData, handleSubmit} = this.props;
        const {activeKey} = this.state;
        return (
            <Fragment>
                <Accordion activeKey={activeKey}>
                    <TermsCard
                        activeKey={activeKey}
                        index="0"
                        isOpen={true}
                        termId='Üyelik'
                        title='Üyelik Sözleşmesi'
                        onCollapse={this.handleAccordionCollapse}
                    >
                        <UyelikSozlesmesi
                            name={userData.name}
                            phone={userData.phone}
                            email={userData.email}
                        />
                    </TermsCard>
                    <TermsCard
                        activeKey={activeKey}
                        index="1"
                        isOpen={false}
                        termId='Gizlilik'
                        title='Gizlilik Politikası'
                        onCollapse={this.handleAccordionCollapse}
                    >
                        <GizlilikPolitikasi/>
                    </TermsCard>
                    <TermsCard
                        activeKey={activeKey}
                        index="2"
                        isOpen={false}
                        termId='Kullanim'
                        title='Kullanım Şartları'
                        onCollapse={this.handleAccordionCollapse}
                    >
                        <KullanimSartlari/>
                    </TermsCard>
                    <TermsCard
                        activeKey={activeKey}
                        index="3"
                        isOpen={false}
                        termId='Aydınlatma'
                        title='Aydınlatma Metni ve Açık Rıza Formu'
                        onCollapse={this.handleAccordionCollapse}
                    >
                        <AydinlatmaFormu
                            name={userData.name}
                            email={userData.email}
                        />
                    </TermsCard>
                </Accordion>
                <form className='user-form' onSubmit={handleSubmit}>
                    <div className='checkbox-wrapper'>
                        <label className='checkbox-container'>
                            <input
                                type='checkbox'
                                name='terms-checkbox'
                                required
                            />
                            <span className='checkbox-checkmark'></span>
                            {translate('term-checkbox')}
                        </label>
                    </div>
                    <button className='n-btn'>
                        {translate('create-btn')}
                    </button>
                </form>
            </Fragment>
        )
    }
}

export default TermsOfService;