import React from 'react'

const GizlilikPolitikasi = () => {
    return (
        <div className='text-justify'>
            <div className='text-center font-weight-bold mb-3'>
                GİZLİLİK İLKELERİ
            </div>
            <p>
                İşbu Gizlilik İlkeleri, üyelik sözleşmesinin tamamlayıcısı ve ayrılmaz bir parçasıdır. www.enbiosis.com internet sitesinde yer alan internet tarayıcıları ve aktif internet bağlantısı üzerinden çalıştırılabilen Mikrobiyom Analizi ve yapay zekâ temelli kişiselleştirilmiş beslenme rehberi hizmetinin (“ENBIOSIS”) sağlayıcısı ENBIOSIS Biyoteknoloji A.Ş. (ENBIOSIS). (“Sağlayıcı”) olarak, ENBIOSIS ’i kullanan kullanıcıların (“Kullanıcı”) gizliliğini korumak temel hedeflerimizden biridir. Bu amaçla, ENBIOSIS Gizlilik İlkeleri (“Gizlilik İlkeleri”), Kullanıcının kişisel verilerinin 6698 Sayılı Kişisel Verilerin Korunması Kanunu (“KVKK”) ile yürürlükteki diğer ilgili mevzuat ile uyumlu bir şekilde işlenmesi ve Kullanıcının veri işleme konusunda bilgilendirmesi için hazırlanmıştır. Ayrıca, ENBIOSIS Çerez İlkeleri de (“Çerez İlkeleri”) işbu Gizlilik İlkeleri’nin tamamlayıcısı ve ayrılmaz bir parçasıdır.
            </p>
            <p>
                ENBIOSIS, işbu Gizlilik İlkeleri’ni KVKK ve yürürlükteki diğer mevzuatta yapılabilecek değişiklikler çerçevesinde her zaman güncelleme hakkını saklı tutar. Mevzuatta değişikliklerin olması halinde işbu Gizlilik İlkeleri’ni yeni mevzuata uygun olarak güncelleyeceğimizi, yapılan güncellemeleri de ENBIOSIS üzerinden her zaman kolaylıkla takip edebileceğinizi size bildirmek isteriz. Bu kapsamda, ENBIOSIS ’i sürekli kullanımınız Gizlilik İlkeleri’ndeki değişikliklere onay verdiğinizi göstermektedir.
            </p>
            <p>
                Bu Gizlilik İlkeleri sadece ENBIOSIS için geçerlidir ve üçüncü taraflara ait internet siteleri kapsam dışındadır. Sizi ilgilendirebileceğini düşündüğümüz diğer internet sitelerine ait bağlantılar (link) verebiliriz. Ancak, internetin doğası gereği, bu internet sitelerinin gizlilik standartlarını garanti edememekteyiz. ENBIOSIS dışındaki platformların içeriğinden dolayı sorumluluk kabul edememekteyiz. Diğer web sitelerine ait linklere bastığınızda, söz konusu web sitesinin gizlilik beyanlarını okumanızı öneririz.
            </p>
            <p className='font-weight-bold'>
                1. Kişisel Verilerin İşleme Amaçları
            </p>
            <p>
                Kişisel verilerinizi, temel olarak bilimsel yöntemler kullanarak mikrobiyom profilinizi çıkarmak ve bu sayede kendi profiliniz için en uygun kişiselleştirilmiş beslenme rehberini yapay zeka teknolojileri başta olmak üzere sair teknolojiler kullanarak aracılığıyla tespit etmek için işleriz. Bir başka deyişle ENBIOSIS uygulamasını kullanarak kişiye özgü beslenme rehberinizi çıkarabilmemiz için aşağıda detaylarını listelediğimiz kişisel verilerinizi işlememiz gerekmektedir. Kişisel verilerin işlenmesinden anlaşılması gereken bu verilerin toplanması, saklanması, kullanılması, istatistiki analizlerinin yapılması ve aktarılması gibi size ait verilerin otomatik yahut otomatik olmayan yollarla işlenmesidir.
            </p>
            <p>
                Yine kişisel verilerinizin ENBIOSIS web sitesi ve uygulamasını kullanmaya devam etmeniz hâlinde beslenme programınızın periyodik olarak takip edilmesi ve mikrobiyotanın sağlıklı hâle getirilmesi için de işlenmesi gerekmektedir.
            </p>
            <p>
                Öte yandan; ENBIOSIS kişiselleştirilmiş beslenme rehberine ek olarak, gelecekte kişiye özel veya kitlesel tedavi yöntemlerinin geliştirilmesi üzerine de bilimsel hazırlık çalışmaları yapmaktadır. Bu kapsamda; kişisel verileriniz bu tür bilimsel çalışmalarda da anonim hale getirilerek kullanılabilir. Bu verilerin kapsamını sağlamış olduğunuz sağlık ve yaşam tarzı beyanları, göndermiş olduğunuz numuneden elde edilen bağırsak mikroorganizmalarının (bakteri, virus, mantar) ve mikrobiyomun analizine yönelik metagenomik, transkriptomik, proteomik veya metabolomik gibi farkliomikeknolojileri, biyoinformatik analizler ve bu numunelerden izole edilerek elde edilen her türlü DNA, RNA, protein ve metabolitlere ait moleküler biyoloji ve biyoinformatik analiz sonuçları oluşturmaktadır. Bu biyolojik materyeller içerisinde mikroorganizma kökenli olanların yanında, dışkı içerisindeki besin artığı kaynaklı olabilecek ve konakçı olarak sizden bulaşmış olabilecek genomik ve matebolomik komponentler de yer almaktadır. Bununla birlikte; ENBIOSIS halihazırda kişiye özel tedavi hizmeti vermemektedir.
            </p>
            <p>
                Miktobiyom analizi ve Kişiselleştirilmiş Beslenme Rehberinin oluşturabilmesi için tarafımıza iletilen dışkı (gaita) örneklerinin laboratuvar ortamında incelenmesi gerekmektedir. Bu inceleme, sırasıyla gaita örneklerinden total DNA’nın izole edilmesi, bu DNA içerisinden sadece bir grup mikroorganizmada (bakteriler ve arkealar) bulunan 16S rRNA geninin polimeraz zincir reaksiyonu işlemiyle amplifiye edilmesi, bu genlerin yeni nesil DNA dizileme yöntemleriyle  dizilenmesi ve dijital veri haline getirilmesi, bu dijital verinin biyoinformatik analizle sizin örneğinizdeki bakteri cinsleri ve bağıl miktarlarını ortaya koyacak şekilde işlenmesi, biyoinformatik analiz sonucunun sizin beyanlarınız ile birlikte ENBIOSIS’e ait analiz algoritması ile işlenerek kişisel beslenme önerileri çıktılarının alınması aşamalarından oluşmaktadır. Sizden alınan dışkı örnekleri ENBIOSIS tarafından size özel bir ID numarası verilerek anomin hale getirildikten sonra işleme alınır. Laboratuvar ortamına örnekleriniz açık kimlik bilgileriniz verilmeden (size özel bir ID numarası verilerek) aktarılır. Laboratuvar sonuçları ise doğrudan, kişiselleştirilmiş beslenme rehberini oluşturulabilmesi için diyetisyenler, hekimler ve hastaneler ile paylaşılır. ENBIOSIS, halihazırda Erciyes Üniversitesi Genom ve Kök Hücre Merkezi ile işbirliği yapmakta olup, ileride yurt içinde yahut yurt dışında başkaca laboratuvarla işbirliği yapabilir.
            </p>
            <p>
                Yukarıdaki ek olarak; ENBIOSIS tarafından elde edilen her türlü kişisel veriniz (özel nitelikli kişisel veriler de dahil fakat bunlarla sınırlı olmamak kaydıyla) aşağıdaki amaçlar ile işlenebilecektir:
            </p>
            <ul>
                <li>Kimliğinizi teyit etme,</li>
                <li>Sağlık hizmetleri ile finansmanının planlanması ve yönetimi,</li>
                <li>İlaç temini,</li>
                <li>Risk yönetimi ve kalite geliştirme aktivitelerinin yerine getirilmesi,</li>
                <li>Sağlık hizmetlerini geliştirme amacıyla analiz yapma,</li>
                <li>Mali İşler, Pazarlama bölümleri tarafından sağlık hizmetlerinizin finansmanı, tetkik ve tedavi giderlerinizin karşılanması, müstehaklık sorgusu kapsamında özel sigorta şirketler ile talep edilen bilgilerin paylaşılması,</li>
                <li>Araştırma yapılması,</li>
                <li>Yasal ve düzenleyici gereksinimlerin yerine getirilmesi,</li>
                <li>Sağlık hizmetlerinin finansmanı kapsamında özel sigorta şirketler ile talep edilen bilgileri paylaşma,</li>
                <li>Kalite, Bilgi Sistemleri bölümleri tarafından risk yönetimi ve kalite geliştirme aktivitelerinin yerine getirilmesi,</li>
                <li>Mali İşler, Pazarlama bölümleri tarafından hizmetlerimiz karşılığında faturalandırma yapılması ve anlaşmalı olan kurumlarla ilişkinizin teyit edilmesi,</li>
                <li>Pazarlama, Medya ve İletişim, Çağrı Merkezi bölümleri tarafından kampanyalara katılım ve kampanya bilgisi verilmesi, Web ve mobil kanallarda özel içeriklerin, somut ve soyut faydaların tasarlanması ve iletilebilmesi.</li>
            </ul>
            <p>
                İlgili mevzuat uyarınca elde edilen ve işlenen Kişisel Verileriniz, ENBIOSIS ‘e ait fiziki arşivler ve/veya bilişim sistemlerine nakledilerek, hem dijital ortamda hem de fiziki ortamda muhafaza altında tutulabilecektir.
            </p>
            <p className='font-weight-bold'>
                2. Kişisel Verilen Elde Edilmesi
            </p>
            <p>
                Özel nitelikli kişisel sağlık verileriniz kurye ile tarafınıza gönderdiğimiz kit ile bağırsak mikrobiyomunuzu içeren dışkı örneğinizin kendi tarafınızdan alınarak yine kurye ile bize iletmeniz yoluyla elde edilir. Tarafınıza ilettiğimiz kitte dışkı örneğinizin nasıl alınacağı, paketleneceği ve tarafımıza gönderileceği adım adım anlatılmaktadır.
            </p>
            <p>
                Öte yandan yine hassas nitelikli sağlık verileriniz; kit ile birlikte tarafınıza gönderdiğimiz anketin doldurulması yoluyla da yazılı olarak elde edilir.
            </p>
            <p>
                Bunlara ek olarak; mobil uygulamamız, internet sitemiz, telefon numaralarımız gibi sesli, yazılı, görsel yollarla da kişisel verileriniz tarafımızca toplanmaktadır. 
            </p>
            <p className='font-weight-bold'>
                3. Topladığımız Kişisel Veriler
            </p>
            <p>
                Sağlık verileriniz başta olmak üzere özel nitelikli kişisel verileriniz ve genel nitelikli kişisel verileriniz, ENBIOSIS tarafından aşağıda yer alanlar dâhil ve bunlarla sınırlı olmaksızın bu maddede belirtilen amaçlar ile bağlantılı, sınırlı ve ölçülü şekilde işlenebilmektedir:
            </p>
            <ul>
                <li>Mikrobiyom (gaita) örnekleri,</li>
                <li>Tarafınıza ilettiğimiz ekteki ankette doldurduğunuz tüm bilgiler;</li>
                <li>Tarafınıza ilettiğimiz ekteki ankette doldurduğunuz tüm bilgiler;</li>
                <li>Sağlık geçmişi (geçmiş hastalık şikayetleriniz, diyabet, karaciğer, bağırsak vs.)</li>
                <li>Alerji bilgisi (gıda ve sair başkaca alerjiler)</li>
                <li>İlaç kullanım bilgisi ve geçmişi (antibiyotikler, grip aşısı, vitaminler vs.)</li>
                <li>Beslenme alışkanlıklarınız (beslenme tipi, içecekler, et, sebze, meyve tüketimi vs.)</li>
                <li>Kimlik bilgileriniz: Adınız, soyadınız, T.C. Kimlik numaranız, pasaport numaranız veya geçici T.C. Kimlik numaranız, doğum yeri ve tarihiniz, medeni haliniz, cinsiyetiniz, sigorta veya hasta protokol numaranız ve sizi tanımlayabileceğimiz diğer kimlik verileriniz.</li>
                <li>İletişim Bilgileriniz: Adresiniz, telefon numaranız, elektronik posta adresiniz ve sair iletişim verileriniz, müşteri temsilcileri ya da hasta hizmetleri tarafından çağrı merkezi standartları gereği tutulan sesli görüşme kayıtlarınız ile elektronik posta, mektup veya sair vasıtalar aracılığı ile tarafımızla iletişime geçtiğinizde elde edilen kişisel verileriniz.</li>
                <li>Muhasebesel Bilgileriniz: Banka hesap numaranız, IBAN numaranız, kredi kartı bilginiz, faturalama bilgileriniz gibi finansal verileriniz.</li>
                <li>Sağlık hizmetlerinin finansmanı ve planlaması amacıyla özel sağlık sigortasına ilişkin verileriniz ve Sosyal Güvenlik Kurumu verileriniz.</li>
                <li>Sağlık Bilgileriniz: Laboratuvar sonuçlarınız, test sonuçlarınız, muayene verileriniz, check-up bilgileriniz, danışan-diyet bilgileriniz dahil ancak bunlarla sınırlı olmaksızın önerilen diyet sırasında veya bunların bir sonucu olarak elde edilen her türlü sağlık ve kişisel verileriniz.</li>
                <li>www.enbiosis.com.tr sitesine veya ENBIOSIS mobil uygulaması üzerinden gönderdiğiniz veya girdiğiniz sağlık verileriniz ve sair kişisel verileriniz.</li>
            </ul>
            <p className='font-weight-bold'>
                4. Kişisel Verilerin Aktarılması
            </p>
            <p>
                Kişisel verileriniz, Kanun ve sair mevzuat kapsamında ve yukarıda yer verilen amaçlarla ENBIOSIS ile, Özel sigorta şirketleri, Sağlık bakanlığı ve bağlı alt birimleri, Sosyal Güvenlik Kurumu, Türkiye Eczacılar Birliği ve sair üçüncü kişiler, yetki vermiş olduğunuz temsilcileriniz, avukatlar, vergi ve finans danışmanları ve denetçiler de dâhil olmak üzere danışmanlık aldığımız üçüncü kişiler, düzenleyici ve denetleyici kurumlar, resmi merciler dâhil sağlık hizmetlerini yukarıda belirtilen amaçlarla geliştirmek veya yürütmek üzere işbirliği yaptığımız iş ortaklarımız ve diğer üçüncü kişiler ile paylaşılabilecektir.
            </p>
            <p>
                Kişisel verileriniz reklam veren şirketlerle paylaşılmaz, sırf reklam maksatlı kullanılmaz. ENBIOSIS personeli dışında üçüncü kişilere bilimsel araştırma yapması için yahut kendi ticari faaliyetlerini yürütmesi için aktarılmaz.
            </p>
            <p>
                Kişisel verileriniz:
            </p>
            <p>
                (a)	Yukarıda KVKK Madde 5.2 ve Madde 6.3 kapsamında belirlenen amaçların varlığı halinde, yurtiçinde ve başta AB ülkeleri, Amerika, İngiltere, OECD ülkeleri, Hindistan, Çin ve Rusya olmak üzere yurtdışında bulunan depolama, arşivleme, bilişim teknolojileri desteği (sunucu, hosting, program, bulut bilişim), güvenlik, çağrı merkezi gibi alanlarda destek aldığımız üçüncü kişilerle, işbirliği yapılan ve/veya hizmet alınan Grup Şirketleri, iş ortakları, tedarikçi firmalar, bankalar, finans kuruluşları, hukuk, vergi vb. benzeri alanlarda destek alınan danışmanlık firmaları ve belirlenen amaçlarla aktarımın gerekli olduğu diğer ilişkili taraflara aktarılabilmektedir;
            </p>
            <p>
                (b)	Yukarıda KVKK Madde 5.1 ve Madde 6.2 kapsamında yurtiçinde ve başta AB ülkeleri, Amerika, İngiltere, OECD ülkeleri, Hindistan, Çin ve Rusya olmak üzere yurtdışında bulunan belirlenen amaçlar bakımından açık rızanızın alınması şartı ile, pazarlama şirketleri, Grup Şirketleri, pazarlama desteği veren üçüncü kişi hizmet firmalarına (e-posta gönderimi, kampanya oluşturulması amacı ile reklam firmaları, CRM desteği veren firmalara açık rızanıza istinaden aktarılabilmektedir.
            </p>
            <p>
                Ayrıca açık rızanıza istinaden ENBIOSIS ’in bir kısmının veya varlıklarının (marka, alan adı ve sair ticari işletme unsurları dahil ancak bunlarla sınırlı olmaksızın) satılması durumunda Kişisel Verileriniz devralan gerçek ya da tüzel kişilere, bunların paydaşları, iş ortakları, aracıları, danışmanları da dahil olmak üzere yurtiçi ve yurtdışındaki üçüncü kişilere devrin gerektirdiği ölçüde aktarılabilir, işlem sürecinde bu üçüncü kişiler tarafından gerekli değerlendirmenin yapılması ile sınırlı ölçüde işlenebilir ve devir halinde devralan taraf varlıklarla birlikte bu varlıklara bağlı değerler olan Kişisel Verilerinizi kendisi veri sorumlusu olacak şekilde işlemeye devam edebilir.
            </p>
            <p className='font-weight-bold'>
                5. Kişisel Veri Elde Etmenin Yöntemi ve Hukuki Sebebi
            </p>
            <p>
                Kişisel verileriniz, her türlü sözlü, yazılı, görsel ya da elektronik ortamda, yukarıda yer verilen amaçlar ve ENBIOSIS ’in faaliyet konusuna dahil her türlü işin yasal çerçevede yürütülebilmesi ve bu kapsamda ENBIOSIS ’in akdi ve kanuni yükümlülüklerini tam ve gereği gibi ifa edebilmesi için toplanmakta ve işlenmektedir. İşbu kişiler verilerinizin toplanmasının hukuki sebebi;
            </p>
            <ul>
                <li>6698 sayılı Kişisel Verilerin Korunması Kanunu,</li>
                <li>3359 sayılı Sağlık Hizmetleri Temel Kanunu,</li>
                <li>663 sayılı Sağlık Bakanlığı ve Bağlı Kuruluşlarının Teşkilat ve Görevleri Hakkında Kanun Hükmünde Kararname,</li>
                <li>Özel Hastaneler Yönetmeliği,</li>
                <li>Kişisel Sağlık Verileri Hakkında Yönetmelik,</li>
                <li>Sağlık Bakanlığı düzenlemeleri ve sair mevzuat hükümleridir.</li>
                <li>Avrupa Birliği Genel Veri Koruma Tüzüğü (GDPR)</li>
            </ul>
            <p>
                Ayrıca, Kanun’un 6. maddesi 3. fıkrasında da belirtildiği üzere sağlık ve cinsel hayata ilişkin kişisel veriler ise ancak kamu sağlığının korunması, koruyucu hekimlik, tıbbı teşhis, tedavi ve bakım hizmetlerinin yürütülmesi, sağlık hizmetleri ile finansmanının planlanması ve yönetimi amacıyla, sır saklama yükümlülüğü altında bulunan kişiler veya yetkili kurum ve kuruluşlar tarafından ilgilinin açık rızası aranmaksızın işlenebilir.
            </p>
            <p className='font-weight-bold'>
                6. Kişisel Verilerin Korunmasına Yönelik Haklar ve Yükümlülükler
            </p>
            <p>
                Kanun ve ilgili mevzuatlar uyarınca;
            </p>
            <ul>
                <li>Kişisel veri işlenip işlenmediğini öğrenme,</li>
                <li>Kişisel veriler işlenmişse buna ilişkin bilgi talep etme,</li>
                <li>Kişisel sağlık verilerine erişim ve bu verileri isteme,</li>
                <li>Kişisel verilerin işlenme amacını ve bunların amacına uygun kullanılıp kullanılmadığını öğrenme,</li>
                <li>Yurt içinde veya yurt dışında kişisel verilerin aktarıldığı üçüncü kişileri bilme,</li>
                <li>Kişisel verilerin eksik veya yanlış işlenmiş olması hâlinde bunların düzeltilmesini isteme,</li>
                <li>Kişisel verilerin silinmesini veya yok edilmesini isteme,</li>
                <li>Kişisel verilerin eksik veya yanlış işlenmiş olması hâlinde bunların düzeltilmesine ve/veya kişisel verilerin silinmesini veya yok edilmesine ilişkin işlemlerin kişisel verilerin aktarıldığı üçüncü kişilere bildirilmesini isteme,</li>
                <li>İşlenen verilerin münhasıran otomatik sistemler vasıtasıyla analiz edilmesi suretiyle kişinin kendisi aleyhine bir sonucun ortaya çıkmasına itiraz etme hakkını haizsiniz.</li>
            </ul>
            <p>
                Mezkûr haklarınızdan birini ya da birkaçını kullanmanız halinde ilgili bilgi tarafınıza, açık ve anlaşılabilir bir şekilde yazılı olarak ya da elektronik ortamda, tarafınızca sağlanan iletişim bilgileri yoluyla, bildirilir.
            </p>
            <p>
                Tarafınız ile kurulmuş olan sözleşmesel ilişki ve sözleşmenin ifası kapsamında kişisel verilerinizi işleyeceğimizden gerek duyulması halinde yurt içi ve yurt dışı ile paylaşılacağı, bu doğrultuda talep ettiğiniz takdirde kişisel veri işleme, aktarma, muhafaza, kullanım amacı, düzeltme, itiraz etme, silme, yok etme, anonimleştirme, zarar doğması halinde giderim talep etme hakkınız bulunduğunu bildirir, bu hususlarda kanun gereğince tarafımıza başvuru yapabilme hakkına haizsiniz.
            </p>
            <p className='font-weight-bold'>
                7. Veri Güvenliği
            </p>
            <p>
                ENBIOSIS, kişisel verilerinizi bilgi güvenliği standartları ve prosedürleri gereğince alınması gereken tüm teknik ve idari güvenlik kontrollerine tam uygunlukla korumaktadır. Söz konusu güvenlik tedbirleri, teknolojik imkânlar da göz önünde bulundurularak muhtemel riske uygun bir düzeyde sağlanmaktadır. Şirketimiz ile paylaşmış olduğunuz kişisel verilerinizi, veri sorumlusu ENBIOSIS Biyoteknoloji A.Ş. olarak kanun ve mevzuata olarak kullanarak muhafaza edeceğiz.
            </p>
            <p className='font-weight-bold'>
                KİŞİSEL VERİLERİN TOPLANMA YÖNTEMLERİ
            </p>
            <p>
                Kişisel veriler, her türlü sözlü, yazılı ya da elektronik ortamda toplanabilmektedir.
            </p>
            <p className='font-weight-bold'>
                KULLANICININ HAKLARI
            </p>
            <p>
                KVKK’nın 11. maddesi uyarınca Kullanıcı;
            </p>
            <p>
                Kişisel veri işlenip işlenmediğini öğrenme;
            </p>
            <p>
                Kişisel verileri işlenmişse buna ilişkin bilgi talep etme;
            </p>
            <p>
                (c)Kişisel verilerin işlenme amacını ve bunların amacına uygun kullanılıp kullanılmadığını öğrenme;
            </p>
            <p>
                Yurt içinde veya yurt dışında kişisel verilerin aktarıldığı üçüncü kişileri bilme;
            </p>
            <p>
                Kişisel verilerin eksik veya yanlış işlenmiş olması hâlinde bunların düzeltilmesini isteme ve bu kapsamda yapılan işlemin kişisel verilerin aktarıldığı üçüncü kişilere bildirilmesini isteme;
            </p>
            <p>
                KVKK ve ilgili diğer kanun hükümlerine uygun olarak işlenmiş olmasına rağmen, işlenmesini gerektiren sebeplerin ortadan kalkması hâlinde kişisel verilerin silinmesini veya yok edilmesini isteme ve bu kapsamda yapılan işlemin kişisel verilerin aktarıldığı üçüncü kişilere bildirilmesini isteme;
            </p>
            <p>
                İşlenen verilerin münhasıran otomatik sistemler vasıtasıyla analiz edilmesi suretiyle kişinin kendisi aleyhine bir sonucun ortaya çıkmasına itiraz etme;
            </p>
            <p>
                Kişisel verilerin kanuna aykırı olarak işlenmesi sebebiyle zarara uğraması hâlinde zararın giderilmesini talep etme haklarına sahiptir.
            </p>
            <p>
                Kullanıcı olarak, haklarınıza ilişkin taleplerinizi, aşağıda düzenlenen yöntemlerle tarafımıza iletmeniz durumunda, talebiniz, talebin niteliğine göre talebi en geç otuz gün içinde ücretsiz olarak sonuçlandıracaktır. Ancak, Kişisel Verileri Koruma Kurulunca bir ücret öngörülmesi halinde, tarafımızca belirlenen tarifedeki ücret alınacaktır.
            </p>
            <p>
                Yukarıda belirtilen haklarınızı kullanmak için kimliğinizi tespit edici gerekli bilgiler ile KVKK’nın 11. maddesinde belirtilen haklardan kullanmayı talep ettiğiniz hakkınıza/haklarınıza yönelik açıklamalarınızı içeren yazılı talebinizi;
            </p>
            <p>
                ENBIOSIS Biyoteknoloji Anonim Şirketi Maslak Mah. Taşyoncası Sk. Maslak 1453 1 U T4A T4B No: 1 U İç Kapı No:B91 Sarıyer/İSTANBUL adresine kimliğinizi tespit edici belgeler ile bizzat elden iletebilir, noter kanalıyla veya KVKK ’da belirtilen diğer yöntemler ile gönderebilir veya yazılı talebinizi info@enbiosis.com adresine güvenli elektronik imzalı olarak gönderebilirsiniz.
            </p>
            <p className='font-weight-bold'>
                ÇEREZ İLKELERİ
            </p>
            <p>
                İşbu ENBIOSIS Çerez İlkeleri (“Çerez İlkeleri”) Gizlilik İlkeleri’nin tamamlayıcısı ve ayrılmaz bir parçasıdır.
            </p>
            <p>
                Çoğu internet sitesinde olduğu gibi, ENBIOSIS içerisinde, kullanıcıya kişisel içerik ve reklamlar göstermek, site içinde analitik faaliyetler gerçekleştirmek ve ziyaretçi kullanım alışkanlıklarını takip etmek amacıyla çerezler kullanılabilmektedir.
            </p>
            <p>
                ENBIOSIS, işbu Çerez İlkeleri’ni ENBIOSIS ’te hangi çerezlerin kullanıldığını ve Kullanıcının bu konudaki seçimini nasıl yönetebileceğini açıklamak amacıyla hazırlamıştır.
            </p>
            <p className='font-weight-bold'>
                ÇEREZ (COOKIE) TANIMI
            </p>
            <p>
                Çerezler, kullanıcının sabit diskinde geçici olarak kaydedilen ve kullanıcının ENBIOSIS ’i bir sonraki ziyaretinde, ENBIOSIS ’in kullanıcının bilgisayarını tanımasını sağlayan küçük dosyalardır. Çerezler, kullanıcının ziyaret ettiği internet sitesiyle ilişkili sunucular tarafından oluşturulurlar. Böylelikle kullanıcı aynı internet sitesini ziyaret ettiğinde sunucu bunu anlayabilir. Çerezler sadece ENBIOSIS ’i ilgilendiren bilgileri toplamak için kullanır.
            </p>
            <p className='font-weight-bold'>
                ENBIOSIS’DA KULLANILAN ÇEREZLER
            </p>
            <p>
                Çerezleri, analitik / performans, işlevsel amaçlar ve sitemizin güvenli alanlarına erişme becerisi için kullanırız.
            </p>
            <p>
                İzleme Teknolojileri
            </p>
            <p>
                Çerezler ENBIOSIS ve pazarlama aracımız (Google Analytics) tarafından kullanılır. Bu teknolojiler eğilimleri analiz etmek, siteyi yönetmek, kullanıcıların sitelerdeki hareketlerini izlemek ve kullanıcı tabanımız hakkında demografik bilgileri toplamak için kullanılmaktadır.
            </p>
            <p>
                Analytics / Günlük Dosyaları
            </p>
            <p>
                Günlük Dosyaları – Çoğu web sitesinde olduğu gibi, belirli bilgileri otomatik olarak toplarız ve günlük dosyalarına kaydederiz. Bu bilgi, internet protokolü (IP) adreslerini, tarayıcı türünü, internet servis sağlayıcısını (ISS), yönlendiren / çıkan sayfaları, işletim sistemini, tarih / saat damgasını ve / veya tıklama akışı verilerini içerebilir.
            </p>
            <p>
                Bu otomatik olarak toplanan günlük bilgilerini, sizin hakkınızda topladığımız diğer bilgilerle birleştirebiliriz. Bunu size sunduğumuz hizmetleri iyileştirmek, pazarlamayı geliştirmek ve bağlantı geçmişiniz hakkında şeffaflık sağlamak için yapıyoruz.
            </p>
            <p className='font-weight-bold'>
                ÇEREZLERİN KULLANIM AMAÇLARI
            </p>
            <p>
                ENBIOSIS ’te, çerezler aşağıdaki amaçlar kapsamında kullanılmaktadır:
            </p>
            <p>
                ENBIOSIS ’in çalışması için gerekli temel fonksiyonları gerçekleştirmek. Örneğin, Kullanıcının ENBIOSIS 'te farklı sayfaları ziyaret ederken tekrar şifre girmesine gerek kalmaması,
            </p>
            <p>
                ENBIOSIS ’i analiz etmek ve performansını arttırmak. Örneğin, ENBIOSIS ’in üzerinde çalıştığı farklı sunucuların entegrasyonu, ENBIOSIS ’i ziyaret edenlerin sayısının tespit edilmesi ve buna göre performans ayarlarının yapılması ya da ziyaretçilerin aradıklarını bulmalarının kolaylaştırılması,
            </p>
            <p>
                ENBIOSIS ’in işlevselliğini arttırmak ve kullanım kolaylığı sağlamak. Örneğin, ENBIOSIS üzerinden üçüncü taraf sosyal medya mecralarına paylaşımda bulunmak, ENBIOSIS ’i ziyaret eden ziyaretçinin daha sonraki ziyaretinde kullanıcı adı bilgisinin ya da arama sorgularının hatırlanması,
            </p>
            <p>
                Kişiselleştirme, hedefleme ve reklamcılık faaliyeti gerçekleştirmek. Örneğin, ziyaretçilerin görüntüledikleri sayfa ve ürünler üzerinden ziyaretçilerin ilgi alanlarıyla bağlantılı reklam gösterilmesi.
            </p>
            <p className='font-weight-bold'>
                ÇEREZ TERCİHLERİNİN YÖNETİMİ
            </p>
            <p>
                Birçok internet tarayıcısı, varsayılan olarak çerezleri otomatik olarak kabul etmeye ayarlıdır. Bu ayarları, çerezleri engelleyecek veya cihazınıza çerez gönderildiğinde uyarı verecek şekilde değiştirebilirsiniz. Çerezleri yönetmenin birkaç yolu bulunmaktadır. Tarayıcı ayarlarınızı nasıl düzenleyeceğiniz hakkında ayrıntılı bilgi almak için lütfen tarayıcınızın talimat veya yardım ekranına başvurun. Çerezler, üyeler/kullanıcılar tarafından devre dışı bırakabilir ya da engellenebilir. Çerezleri reddederseniz, sitemizin bazı özelliklerini veya alanlarını kullanma yeteneğiniz sınırlı olabilir.
            </p>
        </div>
    )
}

export default GizlilikPolitikasi