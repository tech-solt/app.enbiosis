import React from 'react'

const KullanimSartlari = () => {
    return (
        <div className='text-justify'>
            <div className='text-center font-weight-bold mb-3'>
                SİTE KULLANIM ŞARTLARI
            </div>
            <p>
                Lütfen sitemizi kullanmadan evvel site kullanım şartlarını dikkatlice okuyunuz. 
            </p>
            <p>
                Bu alışveriş sitesini kullanan ve alışveriş yapan müşterilerimiz aşağıdaki şartları kabul etmiş varsayılmaktadır:
            </p>
            <p>
                Sitemizdeki web sayfaları ve ona bağlı tüm sayfalar (‘site’) www.enbiosIs.com adresindeki ENBIOSIS Biyoteknoloji Anonim Şirketi 'nin (ENBIOSIS) malıdır ve onun tarafından işletilir. Sizler (‘Kullanıcı’) sitede sunulan tüm hizmetleri kullanırken aşağıdaki şartlara tabi olduğunuzu, sitedeki hizmetten yararlanmakla ve kullanmaya devam etmekle; Bağlı olduğunuz yasalara göre sözleşme imzalama hakkına, yetkisine ve hukuki ehliyetine sahip ve 18 yaşın üzerinde olduğunuzu, bu sözleşmeyi okuduğunuzu, anladığınızı ve sözleşmede yazan şartlarla bağlı olduğunuzu kabul etmiş sayılırsınız.
            </p>
            <p>
                İşbu sözleşme taraflara sözleşme konusu site ile ilgili hak ve yükümlülükler yükler ve taraflar işbu sözleşmeyi kabul ettiklerinde bahsi geçen hak ve yükümlülükleri eksiksiz, doğru, zamanında, işbu sözleşmede talep edilen şartlar dâhilinde yerine getireceklerini beyan ederler.
            </p>
            <p className='font-weight-bold'>
                1. Sorumluluklar
            </p>
            <p>
                a. ENBIOSIS, fiyatlar ve sunulan ürün ve hizmetler üzerinde değişiklik yapma hakkını her zaman saklı tutar. 
            </p>
            <p>
                b. ENBIOSIS, üyenin sözleşme konusu hizmetlerden, teknik arızalar dışında yararlandırılacağını kabul ve taahhüt eder.
            </p>
            <p>
                c. Kullanıcı, sitenin kullanımında tersine mühendislik yapmayacağını ya da bunların kaynak kodunu bulmak veya elde etmek amacına yönelik herhangi bir başka işlemde bulunmayacağını aksi halde ve 3. Kişiler nezdinde doğacak zararlardan sorumlu olacağını, hakkında hukuki ve cezai işlem yapılacağını peşinen kabul eder. 
            </p>
            <p>
                d. Kullanıcı, site içindeki faaliyetlerinde, sitenin herhangi bir bölümünde veya iletişimlerinde genel ahlaka ve adaba aykırı, kanuna aykırı, 3. Kişilerin haklarını zedeleyen, yanıltıcı, saldırgan, müstehcen, pornografik, kişilik haklarını zedeleyen, telif haklarına aykırı, yasa dışı faaliyetleri teşvik eden içerikler üretmeyeceğini, paylaşmayacağını kabul eder. Aksi halde oluşacak zarardan tamamen kendisi sorumludur ve bu durumda ‘Site’ yetkilileri, bu tür hesapları askıya alabilir, sona erdirebilir, yasal süreç başlatma hakkını saklı tutar. Bu sebeple yargı mercilerinden etkinlik veya kullanıcı hesapları ile ilgili bilgi talepleri gelirse paylaşma hakkını saklı tutar.
            </p>
            <p>
                e. Sitenin üyelerinin birbirleri veya üçüncü şahıslarla olan ilişkileri kendi sorumluluğundadır.
            </p>
            <p className='font-weight-bold'>
                2. Fikri Mülkiyet Hakları
            </p>
            <p>
                2.1. İşbu Site ’de yer alan unvan, işletme adı, marka, patent, logo, tasarım, bilgi ve yöntem gibi tescilli veya tescilsiz tüm fikri mülkiyet hakları site işleteni ve sahibi ENBIOSIS 'e veya belirtilen ilgilisine ait olup ulusal ve uluslararası hukukun koruması altındadır. İşbu Site ’nin ziyaret edilmesi veya bu Site ’deki hizmetlerden yararlanılması söz konusu fikri mülkiyet hakları konusunda hiçbir hak vermez.
            </p>
            <p>
                2.2. Site’de yer alan bilgiler hiçbir şekilde çoğaltılamaz, yayınlanamaz, kopyalanamaz, sunulamaz ve/veya aktarılamaz. Site ’nin bütünü veya bir kısmı diğer bir internet sitesinde izinsiz olarak kullanılamaz.
            </p>
            <p className='font-weight-bold'>
                3. Kayıt ve Güvenlik 
            </p>
            <p>
                Kullanıcı doğru, eksiksiz ve güncel kayıt bilgilerini vermek zorundadır. Aksi halde bu Sözleşme ihlal edilmiş sayılacak ve Kullanıcı bilgilendirilmeksizin hesap kapatılabilecektir.
            </p>
            <p>
                Kullanıcı, site ve üçüncü taraf sitelerdeki şifre ve hesap güvenliğinden kendisi sorumludur.
            </p>
            <p>
                Aksi halde oluşacak veri kayıplarından ve güvenlik ihlallerinden veya donanım ve cihazların zarar görmesinden ENBIOSIS sorumlu tutulamaz.
            </p>
            <p className='font-weight-bold'>
                4. Mücbir Sebep
            </p>
            <p>
                Tarafların kontrolünde olmayan; tabii afetler, yangın, patlamalar, iç savaşlar, savaşlar, ayaklanmalar, halk hareketleri, seferberlik ilanı, grev, lokavt ve salgın hastalıklar, altyapı ve internet arızaları, elektrik kesintisi gibi sebeplerden (aşağıda birlikte "Mücbir Sebep” olarak anılacaktır.) dolayı sözleşmeden doğan yükümlülükler taraflarca ifa edilemez hale gelirse, taraflar bundan sorumlu değildir. Bu sürede Taraflar ’ın işbu Sözleşme ’den doğan hak ve yükümlülükleri askıya alınır.
            </p>
            <p className='font-weight-bold'>
                5. Sözleşmenin Bütünlüğü ve Uygulanabilirlik
            </p>
            <p>
                İşbu sözleşme şartlarından biri kısmen veya tamamen geçersiz hale gelirse, sözleşmenin geri kalanı geçerliliğini korumaya devam eder.
            </p>
            <p className='font-weight-bold'>
                6. Sözleşmede Yapılacak Değişiklikler
            </p>
            <p>
                ENBIOSIS, dilediği zaman sitede sunulan hizmetleri ve işbu sözleşme şartlarını kısmen veya tamamen değiştirebilir. Değişiklikler sitede yayınlandığı tarihten itibaren geçerli olacaktır. Değişiklikleri takip etmek Kullanıcı ’nın sorumluluğundadır. Kullanıcı, sunulan hizmetlerden yararlanmaya devam etmekle bu değişiklikleri de kabul etmiş sayılır.
            </p>
            <p className='font-weight-bold'>
                7. Tebligat
            </p>
            <p>
                İşbu Sözleşme ile ilgili taraflara gönderilecek olan tüm bildirimler, ENBIOSIS 'in bilinen elektronik posta adresi ve kullanıcının üyelik formunda belirttiği elektronik posta adresi vasıtasıyla yapılacaktır. Kullanıcı, üye olurken belirttiği adresin geçerli tebligat adresi olduğunu, değişmesi durumunda 5 gün içinde yazılı olarak diğer tarafa bildireceğini, aksi halde bu adrese yapılacak tebligatların geçerli sayılacağını kabul eder.
            </p>
            <p className='font-weight-bold'>
                8. Delil Sözleşmesi
            </p>
            <p>
                Taraflar arasında işbu sözleşme ile ilgili işlemler için çıkabilecek her türlü uyuşmazlıklarda Taraflar ’ın defter, kayıt ve belgeleri ile ve bilgisayar kayıtları ve faks kayıtları 6100 sayılı Hukuk Muhakemeleri Kanunu uyarınca delil olarak kabul edilecek olup, kullanıcı bu kayıtlara itiraz etmeyeceğini kabul eder.
            </p>
            <p className='font-weight-bold'>
                9. Uyuşmazlıkların Çözümü
            </p>
            <p>
                İşbu Sözleşme’nin uygulanmasından veya yorumlanmasından doğacak her türlü uyuşmazlığın çözümünde İstanbul (Merkez) Adliyesi Mahkemeleri ve İcra Daireleri yetkilidir.
            </p>
        </div>
    )
}

export default KullanimSartlari;