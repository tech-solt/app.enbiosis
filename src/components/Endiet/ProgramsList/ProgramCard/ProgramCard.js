import React, { useRef, useState } from 'react'
import './ProgramCard.css'

const ProgramCard = ({translate, program, setLoading, handleGenerateNutritionProgram}) => {
    const [reportUrl, setReportUrl] = useState(null);
    const linkRef = useRef();

    const handleDownloadClick = () => {
        setLoading(true);
        handleGenerateNutritionProgram(program)
            .then(data => {
                setReportUrl(data);
                setLoading(false);
                linkRef.current.click();
            })
            .catch(() => setLoading(false))
    }

    return (
        <div className='pC-container'>
            <div>
                <i className="far fa-calendar-alt pC-icon"></i>
                <span>{program.start_date}</span> - <span>{program.end_date}</span>
            </div>
            <button className='dP-btn' onClick={handleDownloadClick}>
                <i className="far fa-file-pdf pdf-icon"></i>
                {translate('download')}
            </button>
            <a 
                ref={linkRef}
                className='dP-link'
                href={reportUrl}
                download={`${program.start_date}_${program.end_date}.pdf`}
            >
            </a>
        </div>
    )
}

export default ProgramCard;