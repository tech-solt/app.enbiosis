import React, {useState, Fragment} from 'react'
import ProgramCard from './ProgramCard/ProgramCard';


const ProgramsList = ({translate, programs, handleGenerateNutritionProgram}) => {
    const [loading, setLoading] = useState(false);

    const programsList = programs.map(program => 
        <ProgramCard
            key={program.id}
            translate={translate}
            program={program}
            setLoading={setLoading}
            handleGenerateNutritionProgram={handleGenerateNutritionProgram}
        />
    )
    return (
        <Fragment>
            {programsList}
            {loading && 
                <div className='loading'>
                    <div className='loading-logo'></div>
                </div>
            }
        </Fragment>
    )
}

export default ProgramsList;