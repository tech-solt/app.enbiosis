import React, { Component } from 'react'
import { getActiveLanguage, getTranslate } from 'react-localize-redux';
import {connect} from 'react-redux'
import {setActiveSideItem} from '../../../store/actions/general'
import {handleGetMyNutritionPrograms, handleGenerateNutritionProgram} from '../../../store/actions/endiet'
import CardHead from '../../../components/CardHead/CardHead';
import ProgramsList from '../../../components/Endiet/ProgramsList/ProgramsList';

class MyNutritionPrograms extends Component {
    state = {
        loading: this.props.myNutritionPrograms.length === 0,
        errMessage: ''
    }

    componentDidMount(){
        const {activeKit, setActiveSideItem, myNutritionPrograms, handleGetMyNutritionPrograms} = this.props;

        setActiveSideItem('endiet');

        if(myNutritionPrograms.length === 0){
            const {kit_code} = activeKit;
            handleGetMyNutritionPrograms(kit_code)
                .then(() => 
                    this.setState({
                        loading: false,
                        errMessage: ''
                    })
                )
                .catch(err => 
                    this.setState({
                        loading: false,
                        errMessage: err.kit_code
                    })
                )
        }
    }

    componentDidUpdate(prevProps){
        const {activeLang, activeKit, handleGetMyNutritionPrograms} = this.props;
        const {kit_code} = activeKit;

        if(activeLang !== prevProps.activeLang){
            this.setState({
                loading: true
            });
            handleGetMyNutritionPrograms(kit_code)
                .then(() => 
                    this.setState({
                        loading: false,
                        errMessage: ''
                    })
                )
                .catch(err => 
                    this.setState({
                        loading: false,
                        errMessage: err.kit_code
                    })
                )
        }
    }

    render() {
        const {translate, myNutritionPrograms} = this.props;
        const {loading, errMessage} = this.state;
        return (
            loading ? 
                <div className='loading'>
                    <div className='loading-logo'></div>
                </div> : 
                <div className='main-card'>
                    <CardHead
                        headIcon='analiz-sonuclari-icon.png'
                        smallText={translate('endiet-btn')}
                        pText={translate('nP-btn')}
                    />

                    {errMessage !== '' ? 
                        <div
                            style={{fontSize: '1.2rem', textAlign: 'center', textTransform: 'capitalize'}}
                        >
                            <strong>{errMessage}</strong>
                        </div> : 
                        <ProgramsList
                            translate={translate}
                            programs={myNutritionPrograms}
                            handleGenerateNutritionProgram={handleGenerateNutritionProgram}
                        />
                    }
                </div>
        )
    }
}

const mapStateToProps = state => ({
    activeLang: getActiveLanguage(state.localize),
    translate: getTranslate(state.localize),
    activeKit: state.currentUser.activeKit,
    myNutritionPrograms: state.endiet.myNutritionPrograms
})

export default connect(mapStateToProps, {setActiveSideItem, handleGetMyNutritionPrograms})(MyNutritionPrograms);